package no.noroff.java.heroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
    @Test
    void characterCreated_level1_shouldBeLevel1(){
        //Arrange
        Mage mage = new Mage("Erik");
        int expected = 1;
        //Act
        int actual = mage.getLevel();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void characterGainsLevel_level2_shouldBeLevel2(){
        //Arrange
        Rogue roger = new Rogue("Rouge1");
        int expected = 2;
        roger.levelUp();

        //Act
        int actual = roger.getLevel();

        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void characterIsCreated_defaultAttributes_ShouldBeOneOneEight(){
        //Arrange
        Mage mage = new Mage("Erik");

        double[] expected = {1,1,8};
        //Act
        double strength = mage.totalAttributes.getStrength();
        double dexterity = mage.totalAttributes.getDexterity();
        double intelligence = mage.totalAttributes.getIntelligence();
        double[] actual = {strength,dexterity,intelligence};
        //Assert
        assertArrayEquals(expected,actual);
    }

    @Test
    void rangerIsCreated_defaultAttributes_ShouldBeOneSevenOne(){
        //Arrange
        Ranger ranger = new Ranger("Erik");
        double[] expected = {1,7,1};
        //Act
        double strength = ranger.totalAttributes.getStrength();
        double dexterity = ranger.totalAttributes.getDexterity();
        double intelligence = ranger.totalAttributes.getIntelligence();
        double[] actual = {strength,dexterity,intelligence};
        //Assert
        assertArrayEquals(expected,actual);
    }
    @Test
    void rogueIsCreated_defaultAttributes_ShouldBeTwoSixOne(){
        //Arrange
        Rogue rogue = new Rogue("Erik");
        double[] expected = {2,6,1};
        //Act
        double strength = rogue.totalAttributes.getStrength();
        double dexterity = rogue.totalAttributes.getDexterity();
        double intelligence = rogue.totalAttributes.getIntelligence();
        double[] actual = {strength,dexterity,intelligence};
        //Assert
        assertArrayEquals(expected,actual);
    }
    @Test
    void warriorIsCreated_defaultAttributes_ShouldBeFiveTwoOne(){
        //Arrange
        Warrior warrior = new Warrior("Erik");
        double[] expected = {5,2,1};
        //Act
        double strength = warrior.totalAttributes.getStrength();
        double dexterity = warrior.totalAttributes.getDexterity();
        double intelligence = warrior.totalAttributes.getIntelligence();
        double[] actual = {strength,dexterity,intelligence};
        //Assert
        assertArrayEquals(expected,actual);
    }
    @Test
    void mageIsLevelledUp_totalAttributesIncrease_ShouldBeTwoTwoThirteen(){
        //Arrange
        Mage mage = new Mage("Erik");
        double[] expected = {2,2,13};
        mage.levelUp();
        //Act
        double[] actual = {mage.totalAttributes.getStrength(),
        mage.totalAttributes.getDexterity(),
        mage.totalAttributes.getIntelligence()};
        //Assert
        assertArrayEquals(expected,actual);
    }
    @Test
    void RangerIsLevelledUp_totalAttributesIncrease_ShouldBeTwoTwelveTwo(){
        //Arrange
        Ranger ranger = new Ranger("Erik");
        double[] expected = {2,12,2};
        ranger.levelUp();
        //Act
        double[] actual = {ranger.totalAttributes.getStrength(),
                ranger.totalAttributes.getDexterity(),
                ranger.totalAttributes.getIntelligence()};
        //Assert
        assertArrayEquals(expected,actual);
    }
    @Test
    void RogueIsLevelledUp_totalAttributesIncrease_ShouldBeThreeTenTwo(){
        //Arrange
        Rogue rogue = new Rogue("Erik");
        double[] expected = {3,10,2};
        rogue.levelUp();
        //Act
        double[] actual = {rogue.totalAttributes.getStrength(),
                rogue.totalAttributes.getDexterity(),
                rogue.totalAttributes.getIntelligence()};
        //Assert
        assertArrayEquals(expected,actual);
    }
    @Test
    void WarriorIsLevelledUp_totalAttributesIncrease_ShouldEightFourTwo(){
        //Arrange
        Warrior warrior = new Warrior("Erik");
        double[] expected = {8,4,2};
        warrior.levelUp();
        //Act
        double[] actual = {warrior.totalAttributes.getStrength(),
                warrior.totalAttributes.getDexterity(),
                warrior.totalAttributes.getIntelligence()};
        //Assert
        assertArrayEquals(expected,actual);
    }

}