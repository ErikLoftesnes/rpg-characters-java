package no.noroff.java.itemsAndEquipment;

import no.noroff.java.exceptions.InvalidArmorException;
import no.noroff.java.exceptions.InvalidWeaponException;
import no.noroff.java.heroes.Warrior;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Watchable;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {
    @Test
    void characterEquipsHighLevelWeapon_axeLevel2WarriorLevel1_shouldReturnException(){
        //Arrange
        Warrior warrior = new Warrior("Erik");
        Weapon weapon = new Weapon("axe", 2,SlotSpot.WEAPON,WeaponType.AXE);
        String expected = "Your level is not high enough for this weapon";
        //Act
        InvalidWeaponException thrown = Assertions.assertThrows(InvalidWeaponException.class, () ->
                warrior.equip(weapon));
        String actual = thrown.getMessage();
        //Assert
        Assertions.assertEquals(expected,actual);
    }
    @Test
    void characterEquipsHighLevelArmor_plateLevel2WarriorLevel1_shouldReturnInvalidArmorException() {
        //Arrange
        Warrior warrior = new Warrior("Erik");
        Armor armor = new Armor("plate", 2, SlotSpot.BODY, ArmorType.PLATE);
        String expected = "Your level is not high enough for this armor";
        //Act
        InvalidArmorException thrown = Assertions.assertThrows(InvalidArmorException.class, () ->
                warrior.equip(armor));
        String actual = thrown.getMessage();
        //Assert
        Assertions.assertEquals(expected, actual);
    }
    @Test
    void characterEquipWrongWeaponType_warriorEquipBow_shouldReturnInvalidWeaponException(){
        //Arrange
        Warrior warrior = new Warrior("Erik");
        Weapon weapon = new Weapon("bow", 1, SlotSpot.WEAPON,WeaponType.BOW);
        String expected = "Not able to use selected weapon. Invalid type";
        //Act
        InvalidWeaponException thrown = Assertions.assertThrows(InvalidWeaponException.class, () ->
                warrior.equip(weapon));
        String actual = thrown.getMessage();
        //Assert
        Assertions.assertEquals(expected,actual);
    }
    @Test
    void characterEquipWrongArmorType_warriorEquipCloth_shouldReturnInvalidArmorException(){
        //Arrange
        Warrior warrior = new Warrior("Erik");
        Armor armor = new Armor("cloth", 1, SlotSpot.BODY,ArmorType.CLOTH);
        String expected = "Invalid Armor Type";
        //Act
        InvalidArmorException thrown = Assertions.assertThrows(InvalidArmorException.class, () ->
                warrior.equip(armor));
        String actual = thrown.getMessage();
        //Assert
        Assertions.assertEquals(expected,actual);
    }
    @Test
    void characterEquipValidWeapon_warriorEquipAxe_shouldReturnTrue() throws Exception {
        //Arrange
        Warrior warrior = new Warrior("Erik");
        Weapon weapon = new Weapon("axe", 1, SlotSpot.WEAPON,WeaponType.AXE);
        //Act
        warrior.equip(weapon);
        Boolean expected = true;
        Boolean actual = warrior.getEquipment().get(SlotSpot.WEAPON) == weapon;
        //Assert
        assertEquals(expected,actual);
    }
    @Test
    void characterEquipValidArmor_warriorEquipPlate_shouldReturnTrue() throws Exception {
        //Arrange
        Warrior warrior = new Warrior("Erik");
        Armor armor = new Armor("plate", 1, SlotSpot.BODY,ArmorType.PLATE);
        //Act
        warrior.equip(armor);
        Boolean expected = true;
        Boolean actual = warrior.getEquipment().get(SlotSpot.BODY) == armor;
        //Assert
        assertEquals(expected,actual);
    }
    @Test
    void dpsAtLevel1NoWeaponEquiped_warriorLevel1NoWeapon_shouldReturnonepointofive(){
        //Arrange
        Warrior warrior = new Warrior("Erik");
        double expect = 1.05;
        //Act
        double actual = warrior.getCharacterDps();
        //Assert
        assertEquals(expect,actual);
    }
    @Test
    void calculateDpsWithValidWeapon_warriorLvl1WithAxe_shouldReturn() throws Exception {
        //Arrange
        Warrior warrior = new Warrior("Erik");
        Weapon weapon = new Weapon("axe",1,SlotSpot.WEAPON,WeaponType.AXE);
        warrior.equip(weapon);
        double expect = ((8*1)*(1+(5/100.00)));
        //Act
        double actual = warrior.getCharacterDps();
        //Assert
        assertEquals(expect,actual);
    }
    @Test
    void calculateDpsWithValidWeaponAndArmor_warriorLvl1WithAxeAndPlate_shouldReturn() throws Exception {
        //Arrange
        Warrior warrior = new Warrior("Erik");
        Weapon weapon = new Weapon("axe",1,SlotSpot.WEAPON,WeaponType.AXE);
        Armor armor = new Armor("plate", 1,SlotSpot.BODY,ArmorType.PLATE);
        warrior.equip(weapon);
        warrior.equip(armor);

        double expect = (8*1)*(1+((5+3)/100.00));
        //Act
        double actual = warrior.getCharacterDps();
        //Assert
        assertEquals(expect,actual);
    }
}