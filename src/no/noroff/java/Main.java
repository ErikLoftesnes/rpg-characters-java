package no.noroff.java;

import no.noroff.java.heroes.Hero;
import no.noroff.java.heroes.Mage;
import no.noroff.java.heroes.PrimaryAttributes;
import no.noroff.java.display.characterStatsDisplay;
import no.noroff.java.itemsAndEquipment.*;

public class Main {

    public static void main(String[] args) throws Exception {
	// write your code here
        Mage mage = new Mage("Erik");
        //Level 1
        System.out.println(characterStatsDisplay.showCharacterStats(mage));
        //Level 2
        mage.levelUp();
        System.out.println(characterStatsDisplay.showCharacterStats(mage));
        //Equip staff and armor
        Weapon staff = new Weapon("Staff", 1, SlotSpot.WEAPON, WeaponType.STAFF);
        mage.equip(staff);
        Armor armor = new Armor("Cloth",1,SlotSpot.BODY, ArmorType.CLOTH);
        mage.equip(armor);
        System.out.println(characterStatsDisplay.showCharacterStats(mage));
        //Level 3
        mage.levelUp();
        System.out.println(characterStatsDisplay.showCharacterStats(mage));

    }
}
