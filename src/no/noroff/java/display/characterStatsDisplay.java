package no.noroff.java.display;

import no.noroff.java.heroes.Hero;

public class characterStatsDisplay {
    public static StringBuilder showCharacterStats(Hero hero){
        StringBuilder characterStats = new StringBuilder();
        characterStats.append("Character stats: \n")
                .append("Name: ").append(hero.getName()).append("\n")
                .append("Level: ").append(hero.getLevel()).append("\n")
                .append("Strength: ").append(hero.getTotalAttributes().getStrength()).append("\n")
                .append("Dexterity: ").append(hero.getTotalAttributes().getDexterity()).append("\n")
                .append("Intelligence: ").append(hero.getTotalAttributes().getIntelligence()).append("\n")
                .append("DPS: ").append(hero.getCharacterDps()).append("\n");
        return characterStats;
    }
}
