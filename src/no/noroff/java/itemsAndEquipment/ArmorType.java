package no.noroff.java.itemsAndEquipment;


public enum ArmorType {
    CLOTH, LEATHER, MAIL, PLATE
}
