package no.noroff.java.itemsAndEquipment;

import no.noroff.java.heroes.PrimaryAttributes;

public class Armor extends Item{
    protected ArmorType armorType;
    protected PrimaryAttributes armorAttributes;

    public Armor(String name, int requiredLevel, SlotSpot slot, ArmorType armorType) {
        super(name, requiredLevel, slot);
        this.armorAttributes = new PrimaryAttributes(3,3,3);
        this.armorType = armorType;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public PrimaryAttributes getArmorAttributes(){
        return armorAttributes;
    }

}
