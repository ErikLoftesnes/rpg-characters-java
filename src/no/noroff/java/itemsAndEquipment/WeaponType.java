package no.noroff.java.itemsAndEquipment;

public enum WeaponType {
    AXE, BOW, DAGGER, HAMMER, STAFF, SWORD,WANDS
}
