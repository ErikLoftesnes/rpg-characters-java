package no.noroff.java.itemsAndEquipment;

public enum SlotSpot {
    HEAD, BODY, LEGS, WEAPON
}
