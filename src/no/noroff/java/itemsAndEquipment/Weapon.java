package no.noroff.java.itemsAndEquipment;

public class Weapon extends Item{
    protected WeaponType weaponType;
    protected double damage = 8;
    protected double attackSpeed = 1;
    protected double dps = calculateDps();

    //Constructor / inherit from Item
    public Weapon(String name, int requiredLevel, SlotSpot slot, WeaponType weaponType) {
        super(name, requiredLevel, slot);
        this.weaponType = weaponType;
    }

    //Getters
    public double getDamage() {
        return damage;
    }

    public double getDps() {
        return dps;
    }

    public WeaponType getWeaponType(){
        return weaponType;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public double calculateDps(){
        return damage * attackSpeed;
    }
}
