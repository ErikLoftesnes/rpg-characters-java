package no.noroff.java.itemsAndEquipment;

public abstract class Item {
    protected String name;
    protected int requiredLevel;
    protected SlotSpot slot;

    //Constructor
    public Item(String name, int requiredLevel, SlotSpot slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    public String getName(){
        return name;
    }

    public int getRequiredLevel(){
        return requiredLevel;
    }

    //Slot getter
    public SlotSpot getSlot(){
        return slot;
    }
}
