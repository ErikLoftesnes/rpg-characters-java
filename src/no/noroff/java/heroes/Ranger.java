package no.noroff.java.heroes;

import no.noroff.java.itemsAndEquipment.*;

import java.util.Arrays;
import java.util.HashSet;

public class Ranger extends Hero{

   public Ranger () {
    }

    //Constructor
    public Ranger(String name){
       super (name, new PrimaryAttributes(1,7,1),
               new HashSet<>(Arrays.asList(WeaponType.BOW)),
               new HashSet<>(Arrays.asList(ArmorType.LEATHER, ArmorType.MAIL)));
    }

    @Override
    public void levelUp() {
       baseAttributes.increaseAtt(1,5,1);
       level++;
       characterDps();
    }

    @Override
    public void equip(Item item) throws Exception {
        super.equip(item);
        if(item.getSlot() == SlotSpot.WEAPON){
            characterDps();
        }
    }

    @Override
    public void dealDamage() {

    }

    @Override
    public void characterDps() {
        Item weapon = equipment.get(SlotSpot.WEAPON);
        double weaponDps = 0;
        if(weapon == null) {
            weaponDps = 1;
        } else {
            weaponDps = ((Weapon)weapon).getDps();
        }
        double totalMainPrimaryAttribute = totalAttributes.getDexterity();
        characterDps = (weaponDps *(1+(totalMainPrimaryAttribute/100)));
    }
}
