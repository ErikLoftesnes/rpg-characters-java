package no.noroff.java.heroes;

import no.noroff.java.itemsAndEquipment.*;

import java.util.Arrays;
import java.util.HashSet;

public class Warrior extends Hero{

    public Warrior(){
    }

    public Warrior(String name){
        super(name, new PrimaryAttributes(5,2,1),
                new HashSet<>(Arrays.asList(WeaponType.AXE, WeaponType.HAMMER,WeaponType.SWORD)),
                new HashSet<>(Arrays.asList(ArmorType.MAIL, ArmorType.PLATE)));
    }

    @Override
    public void levelUp() {
        baseAttributes.increaseAtt(3,2,1);
        level++;
        characterDps();
    }

    @Override
    public void equip(Item item) throws Exception {
        super.equip(item);
        if(item.getSlot() == SlotSpot.WEAPON){
            characterDps();
        }
    }

    @Override
    public void dealDamage() {

    }

    @Override
    public void characterDps() {
        Item weapon = equipment.get(SlotSpot.WEAPON);
        double weaponDps = 0;
        if(weapon == null) {
            weaponDps = 1;
        } else {
            weaponDps = ((Weapon)weapon).getDps();
        }
        double totalMainPrimaryAttribute = totalAttributes.getStrength();
        characterDps = (weaponDps *(1+(totalMainPrimaryAttribute/100)));
    }
}
