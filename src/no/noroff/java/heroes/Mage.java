package no.noroff.java.heroes;

import no.noroff.java.itemsAndEquipment.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class Mage extends Hero{

    public Mage(){
    }

    public Mage(String name) {
        super(name,
                new PrimaryAttributes(1,1,8),
                new HashSet<>(Arrays.asList(WeaponType.STAFF, WeaponType.WANDS)),
                new HashSet<>(Arrays.asList(ArmorType.CLOTH)));}


    @Override
    public void levelUp() {
        //baseatt.increase
        baseAttributes.increaseAtt(1,1,5);
        level++;
        characterDps();
    }

    @Override
    public void equip(Item item) throws Exception {
        super.equip(item);
        if(item.getSlot() == SlotSpot.WEAPON){
            characterDps();
        }
    }

    @Override
    public void dealDamage() {
    }

    @Override
    public void characterDps() {
        Item weapon = equipment.get(SlotSpot.WEAPON);
        double weaponDps = 0;
        if(weapon == null) {
            weaponDps = 1;
        } else {
            weaponDps = ((Weapon)weapon).getDps();
        }

        double totalMainPrimaryAttribute = totalAttributes.getIntelligence();
        characterDps = (weaponDps *(1+(totalMainPrimaryAttribute/100)));
    }
}
