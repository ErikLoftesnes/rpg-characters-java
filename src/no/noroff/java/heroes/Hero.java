package no.noroff.java.heroes;
import no.noroff.java.exceptions.InvalidArmorException;
import no.noroff.java.exceptions.InvalidWeaponException;
import no.noroff.java.itemsAndEquipment.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

//
public abstract class Hero {
    protected String name;
    protected int level = 1;
    protected double characterDps;
    protected PrimaryAttributes baseAttributes;
    protected PrimaryAttributes totalAttributes;
    protected HashSet<WeaponType> weapon;
    protected HashSet<ArmorType> armor;
    protected HashMap<SlotSpot, Item> equipment;

    public Hero() {

    }

    //Constructor
    public Hero(String name, PrimaryAttributes baseAttributes,
                HashSet<WeaponType> weapon, HashSet<ArmorType> armor) {
        this.name = name;
        this.baseAttributes = baseAttributes;
        this.weapon = weapon;
        this.armor = armor;
        totalAttributes = baseAttributes;
        initializeEquipment();
        characterDps();
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public PrimaryAttributes getBaseAttributes() {
        return baseAttributes;
    }

    public PrimaryAttributes getTotalAttributes() {
        return totalAttributes;
    }

    public double getCharacterDps(){
        characterDps();
        return characterDps;
    }

    public HashMap<SlotSpot, Item> getEquipment() {
        return equipment;
    }

    //Deal damage. Makes it abstract because every character need to be able to deal damage.
    public abstract void dealDamage();

    //Total attributes
    public void calculateTotalAttributes(){
        Item head = equipment.get(SlotSpot.HEAD);
        Item body = equipment.get(SlotSpot.BODY);
        Item legs = equipment.get(SlotSpot.LEGS);

        Item[] armorItems = {head, body, legs};

        for(Item armorItem : armorItems){
        if (armorItem != null){
            totalAttributes.increaseAtt(((Armor)armorItem).getArmorAttributes().getStrength(),
                    ((Armor)armorItem).getArmorAttributes().getDexterity(),
                    ((Armor)armorItem).getArmorAttributes().getIntelligence());
        }
        }



    }

    //Method for level up
    public void levelUp() {
    }

    public void initializeEquipment() {
        equipment = new HashMap<>();
        equipment.put(SlotSpot.HEAD, null);
        equipment.put(SlotSpot.BODY, null);
        equipment.put(SlotSpot.LEGS, null);
        equipment.put(SlotSpot.WEAPON, null);
    }

    //Equipping item
    public void equip(Item item) throws Exception {
        if (item.getRequiredLevel() > getLevel()) {
            if(item.getSlot() == SlotSpot.WEAPON){
                throw new InvalidWeaponException("Your level is not high enough for this weapon");
            }else {
                throw new InvalidArmorException("Your level is not high enough for this armor");
            }
        }
        if (item.getSlot() == SlotSpot.WEAPON) {
            if (!weapon.contains(((Weapon) item).getWeaponType())) {
                throw new InvalidWeaponException("Not able to use selected weapon. Invalid type");
            }
        } else {
            if (!armor.contains(((Armor) item).getArmorType())) {
                throw new InvalidArmorException("Invalid Armor Type");
            }
        }
        equipment.put(item.getSlot(), item);
        calculateTotalAttributes();
    }

    public abstract void characterDps();

}
