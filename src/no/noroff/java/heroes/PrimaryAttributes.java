package no.noroff.java.heroes;

public class PrimaryAttributes {
    private double strength;
    private double dexterity;
    private double intelligence;

    public PrimaryAttributes(double strength, double dexterity, double intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public double getStrength() {
        return strength;
    }

    public double getDexterity() {
        return dexterity;
    }

    public double getIntelligence() {
        return intelligence;
    }

    //Increase method
    public void increaseAtt(double strength, double dexterity, double intelligence){
        this.strength += strength;
        this.dexterity += dexterity;
        this.intelligence += intelligence;

    }

}
