# RPG Characters Console App Java

Simple overview of use/purpose.
This is the first project of the java backend course at Experis.
Main functions includes creating a character, equipping weapon and armor and level up the character. 
The program is written in Java, using IntelliJ with JDK 17.

## Description
An in-depth paragraph about your project and overview of use.
A user can create a character choosing between mage, rogue, ranger and warrior.
The character will always start at level 1.
The character can equip different weapons and armor depending on its type. 
Each character has main attributes, strength, dexterity and intelligence.
When a character equips something or level up, these attributes are enhanced.

## Getting Started
To create a character the only requirement is to pass in a name. 

### Dependencies
JDK 17
JUnit 5 for testing

### Executing program
Program runs in main

## Authors
Erik Loftesnes
